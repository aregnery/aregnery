# Home office equipment

|Item|Source|Price|
|----|------|-----|
|**Computer**|[16-inch MacBook Pro, Space Gray – 2019](https://www.apple.com/shop/buy-mac/macbook-pro/16-inch-space-gray-2.6ghz-6-core-processor-512gb)|$3200 |
|**Chair** | [Herman Miller - Embody Chair](https://store.hermanmiller.com/office/office-chairs/embody-task-chair/4737.html?lang=en_US&) | $1,650 |
|**Monitor** |[Alienware 34-in Ultrawide, AW3420DW](https://www.dell.com/en-us/shop/new-alienware-34-curved-gaming-monitor-aw3420dw/apd/210-atzq/monitors-monitor-accessories)|$1,125|
|**Headphones** | [Microsoft Surface Headphones, Matte Black](https://www.microsoft.com/en-us/p/surface-headphones-2/8WQ0BSX7G523/CS2R?activetab=pivot%3aoverviewtab) | $275|
|**Desk** | [Autonomous – SmartDesk 2 – White XL Top + Frame](https://www.autonomous.ai/standing-desks/smartdesk-2-home?option1=3&option2=7&option16=36&option17=1881) | $515|  
|**Keyboard** | [Keychron K2 - Red Switches](https://www.keychron.com/products/keychron-k2-wireless-mechanical-keyboard?variant=31063869718617) | $100|
|**Mouse** | [MX Master 2](https://www.amazon.com/Logitech-Master-Wireless-Mouse-Rechargeable/dp/B071YZJ1G1/ref=sr_1_1?dchild=1&keywords=MX+Master+2&qid=1591824548&sr=8-1) | $100|
|**Desk Mat** | [Teather Black Leather Desk Pad (34"x17")](https://www.amazon.com/gp/product/B079QFDY3C/ref=ppx_yo_dt_b_asin_title_o05_s00?ie=UTF8&psc=1) | $34|
|**Cord** | [Display Port <> USB-C](https://www.amazon.com/gp/product/B074V5MMCH/ref=ppx_yo_dt_b_asin_title_o02_s00?ie=UTF8&psc=1) | $20|
|**Heaphone Stand** | [Full Aluminum Headphone Stand](https://www.amazon.com/gp/product/B07Z7H31FQ/ref=ppx_yo_dt_b_asin_title_o00_s00?ie=UTF8&psc=1)| $12|

MacBook Specs

    - 2.4 GHz 8-Core Intel Core i9
    - 32 GB 2667 MHz DDR4
    - AMD Radeon Pro 5500M 4 GB
    - 512 GB SSD




 
  

