### Tell me about your experience of our culture of iteration since joining GitLab - how does it differ from your previous roles?
---
- Breaking things down into really small bits helps provide some psychological saftey
- Stuff _ships_ frequently
- I feel empowered to focus on improving focused areas of GitLab instead of _everything_ with our team
- Developers/PMs/Managers etc. all like to participating in the iteration process
- - I can contribute outside of UX
  - Edited the handbook several times during onboarding
  - Worked with a dev to update some legacy components (buttons/icons)

### What's been interesting? What's been challenging? Where do you think we could improve in our approach?
---
- ~~insteresting~~ Exciting
  - Working alognside very passionate designers that are talented in various ways
  - A welcoming bunch that shares their opinions and offers to help (constructively)
- Challenging
  - Feeling behind while onboarding. The shipping train never stops.
  - jobs to be done
  - Asking questions in the open and not in DM

### What advice would you give to a designer considering joining GitLab?
---
- For a stage or group you're interested in, try looking at that team's board and look for issues they are working on today
- Read the Product Development Flow, and see if it something that you could see yourself working within
- Try making things with our library in Figma, propose a change by opening an issue

### Question for you: is it truly an itertation if no user feedback has been collected to inform decision making?