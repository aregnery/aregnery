## Navigation

### What some things our navigation get right?

- The global navigation of the site shows the top tier of the tree, no matter where the user currently is in the structure. 
- **The global navigation is stable** — users can expect that it will remain the same throughout the site and they can use it to jump between major categories.
- The IA structure of most websites is hierarchical, and **a familiar way to visualize that hierarchy is a tree**.
- The **local navigation shows information about the node** the user is currently viewing and also includes the other nodes on the same branch of the tree. 
- Uses a vertical navigation structure on the left and uses a noticeable design.
- **Specific categories increase findability and decrease interaction cost.**
- **Vertical navigation offers room for growth.**
- **Vertical navigation supports more [efficient](https://locahost.com) scanning than horizontal navigation.**
- Doesn’t duplicate the menu both vertically and horizontally
- Doesn’t hide the navigation behind icons
- Avoids multilevel hierarchical dropdown menus
- It's always instant

### What are the three biggest opportunites I see

1. Combating ~~feature~~ page bloat. Why does almost every page need to be in the left nav?
1. Traversing context. How might we allow users to jump back and forth?
1. Reorienting users. Is there enough sign posting for users, regardless of where they land.

### MVC ideas I'd like to place a bet on

1. Removing the top nav `Menu` will reduce confusion with the left nav
1. Creating a `Workspace` left navigation illustrates an instance level view where some admin features could move in to
1. Defining the IA of settings so that they are not avoided like the plague 
1. Working with each nav section to determine if something needs to be in a sub menu
1. Defining what categories should be in the navigation
1. Allowing users to pick their preferences
1. Single notification center. Something better than keeping track of badge counts in my head.
1. Reducing the number of paths to get to the same content like personal projects



