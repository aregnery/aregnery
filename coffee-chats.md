```json:table
{
    "fields" : [
        {"key": "Person", "label": "Person", "sortable": true},
        {"key": "Profile", "label": "GitLab profile"},
        {"key": "Role", "label": "Role"},
        {"key": "Last chat", "label": "Last chat", "sortable": true},
        {"key": "Count", "label": "Count", "sortable": true}
    ],
    "items" : [
      {
        "Person": "Adam Smolinski 🇺🇸",
        "Profile": "https://gitlab.com/asmolinski2",
        "Role": "Director, UX Research",
        "Last chat": "2020-06-25",
        "Count": "🍩"
      },
      {
        "Person": "Adrian Waters 🇬🇧",
        "Profile": "https://gitlab.com/awaters3",
        "Role": "Solutions Architect (Customer Success)",
        "Last chat": "2020-08-05",
        "Count": "☕️"
      },
      {
        "Person": "Adi Wolf 🇨🇦",
        "Profile": "https://gitlab.com/adiwolff",
        "Role": "Strategic Account Leader - Pacific NW",
        "Last chat": "2020-09-16",
        "Count": "🍩"
    },
    {
        "Person": "Aishwarya Subramanian 🇮🇳",
        "Profile": "https://gitlab.com/asubramanian1",
        "Role": "Backend Engineer, Manage:Compliance",
        "Last chat": "2021-01-07",
        "Count": "☕️☕️☕️"
    },
    {
        "Person": "Alex Fracazo 🇦🇺",
        "Profile": "https://gitlab.com/afracazo",
        "Role": "Product Designer, Compliance",
        "Last chat": "2023-01-11",
        "Count": "☕️"
    },
    {
        "Person": "Alex Martin 🇺🇸",
        "Profile": "https://gitlab.com/alex_martin",
        "Role": "Sr. Manager, Product & Data Analytics",
        "Last chat": "2021-10-04",
        "Count": "🍩"
    },
    {
        "Person": "Alexis Ginsberg 🇺🇸",
        "Profile": "https://gitlab.com/uhlexsis",
        "Role": "Senior Product Designer, Plan:Portfolio Management",
        "Last chat": "2022-03-30",
        "Count": "☕️☕️"
    },
    {
        "Person": "Ali Ndlovu",
        "Profile": "https://gitlab.com/andlovu",
        "Role": "Product Designer, Configure",
        "Last chat": "2022-03-07",
        "Count": "🍩"
    },
    {
        "Person": "Alyson Turner 🇺🇸",
        "Profile": "https://gitlab.com/sebbyturner",
        "Role": "Sales Development Representative",
        "Last chat": "2022-07-22",
        "Count": "🍩"
    },
    {
        "Person": "Amanda Hughes 🇺🇸",
        "Profile": "https://gitlab.com/amandakhughes",
        "Role": "Senior Product Designer, Manage:Import",
        "Last chat": "2020-06-25",
        "Count": "☕️"
    },
    {
        "Person": "Andjay Wieckowski 🇵🇱",
        "Profile": "https://gitlab.com/Andjay",
        "Role": "Senior UX Researcher",
        "Last chat": "2020-08-03",
        "Count": "☕️☕️"
    },
    {
        "Person": "Andrej Kiripolsky 🇨🇿",
        "Profile": "https://gitlab.com/akiripolsky",
        "Role": "UX Researcher – Package, Configure, Monitor",
        "Last chat": "2020-10-29",
        "Count": "🍩"
    },
    {
        "Person": "Andy Volpe 🇺🇸",
        "Profile": "https://gitlab.com/andyvolpe",
        "Role": "Sr. Product Designer - Secure",
        "Last chat": "2021-09-16",
        "Count": "🍩🍩"
    },
    {
        "Person": "Anthony Sandoval 🇺🇸",
        "Profile": "https://gitlab.com/AnthonySandoval",
        "Role": "Engineering Manager, Infrastructure",
        "Last chat": "2020-08-28",
        "Count": "☕️"
    },
    {
        "Person": "Anne Lasch 🇩🇪",
        "Profile": "https://gitlab.com/alasch",
        "Role": "Senior UX Researcher",
        "Last chat": "2020-12-22",
        "Count": "☕️☕️☕️"
    },
    {
        "Person": "Ashley Knobloch 🇺🇸",
        "Profile": "https://gitlab.com/aknobloch",
        "Role": "UX Researcher",
        "Last chat": "2022-01-24",
        "Count": "☕️"
    },
    {
        "Person": "Becka Lippert 🇺🇸",
        "Profile": "https://gitlab.com/beckalippert",
        "Role": "Product Designer, Static Analysis (Secure)",
        "Last chat": "2021-11-30",
        "Count": "🍩"
    },
    {
        "Person": "Ben Leduc-Mills 🇺🇸",
        "Profile": "https://gitlab.com/leducmills",
        "Role": "Senior UX Researcher",
        "Last chat": "2021-12-13",
        "Count": "🍩"
    },
    {
        "Person": "Bistra Lutz 🇺🇸",
        "Profile": "https://gitlab.com/blutz1",
        "Role": "Manager, Security Engineering",
        "Last chat": "2021-03-31",
        "Count": "🍩"
    },
    {
        "Person": "Blaire Christopher 🇺🇸",
        "Profile": "https://gitlab.com/badnewsblair",
        "Role": "Product Design Manager, Plan and Manage",
        "Last chat": "2022-06-17",
        "Count": "☕️☕️"
    },
    {
        "Person": "Bruno Freitas 🇦🇴",
        "Profile": "https://gitlab.com/b_freitas",
        "Role": "Support Engineer (EMEA)",
        "Last chat": "2021-05-27",
        "Count": "🍩"
    },
    {
        "Person": "Bruno Lazzarin 🇦🇷",
        "Profile": "https://gitlab.com/blazzarin",
        "Role": "Sales Development Representative, LATAM",
        "Last chat": "2021-04-14",
        "Count": "🍩"
    },
    {
        "Person": "Camellia Xueyi 🇳🇱",
        "Profile": "https://gitlab.com/cam.x",
        "Role": "Sr. Product Designer, Secure",
        "Last chat": "2022-07-07",
        "Count": "🍩🍩🍩"
    },
    {
        "Person": "Carlos Dominguez  🇺🇸",
        "Profile": "https://gitlab.com/carlos.dominguez",
        "Role": "Strategic Account Leader (Southeast)",
        "Last chat": "2021-04-27",
        "Count": "🍩"
    },
    {
        "Person": "Caitlin Faughnan 🇮🇪",
        "Profile": "https://gitlab.com/cfaughnan",
        "Role": "UX Research Coordinator",
        "Last chat": "2022-04-14",
        "Count": "🍩"
    },
    {
        "Person": "Charlotte Platt",
        "Profile": "https://gitlab.com/cplatt39",
        "Role": "Manager, Business Development",
        "Last chat": "2022-05-16",
        "Count": "🍩"
    },
    {
        "Person": "Chris Micek",
        "Profile": "https://gitlab.com/chrismicek",
        "Role": "Product design manager, Manage & Enablement",
        "Last chat": "2023-01-18",
        "Count": "☕️"
    },
    {
        "Person": "Chris Rennie 🌴",
        "Profile": "https://gitlab.com/crennie",
        "Role": "Federal Strategic Account Leader - DoD",
        "Last chat": "2020-09-02",
        "Count": "☕️"
    },
    {
        "Person": "Christie Lenneville 🇺🇸",
        "Profile": "https://gitlab.com/clenneville",
        "Role": "VP of UX",
        "Last chat": "2021-09-27",
        "Count": "☕️🍩🍩"
    },
    {
        "Person": "Christopher Nelson 🇺🇸",
        "Profile": "https://gitlab.com/ccnelson",
        "Role": "Senior Director, Enterprise Applications",
        "Last chat": "2021-06-23",
        "Count": "🍩"
    },
    {
        "Person": "Coung Ngo 🇬🇧",
        "Profile": "https://gitlab.com/cngo",
        "Role": "Frontend Engineer, Plan",
        "Last chat": "2020-10-19",
        "Count": "🍩"
    },
    {
        "Person": "Craig Miskell 🇳🇿",
        "Profile": "https://gitlab.com/cmiskell",
        "Role": "Site Reliability Engineer",
        "Last chat": "2020-06-25",
        "Count": "☕️"
    },
    {
        "Person": "Dan Jensen 🇺🇸",
        "Profile": "https://gitlab.com/djensen",
        "Role": "Backend Engineering Manager, Manage",
        "Last chat": "2020-10-15",
        "Count": "☕️☕️"
    },
    {
        "Person": "Dan Mizzi-Harris 🇬🇧",
        "Profile": "https://gitlab.com/danmh",
        "Role": "Product Designer, Import & Optimize",
        "Last chat": "2022-03-16",
        "Count": "☕️🍩"
    },
    {
        "Person": "Daniel Fosco 🇳🇱",
        "Profile": "https://gitlab.com/dfosco",
        "Role": "Senior Product Designer, Release",
        "Last chat": "2021-06-01",
        "Count": "☕️☕️🍩"
    },
    {
        "Person": "Daisy Miclat 🇺🇸",
        "Profile": "https://gitlab.com/dmiclat1",
        "Role": "Account Executive, Mid Market",
        "Last chat": "2022-01-26",
        "Count": "🍩"
    },
    {
        "Person": "David Haines 🇦🇺",
        "Profile": "https://gitlab.com/dhaines1",
        "Role": "Strategic Account Leader",
        "Last chat": "2022-03-21",
        "Count": "🍩"
    },
    {
        "Person": "Devin Harris 🌴",
        "Profile": "https://gitlab.com/dsharris",
        "Role": "Senior Security Analyst",
        "Last chat": "2020-09-02",
        "Count": "☕️"
    },
    {
        "Person": "Dennis Tang 🇺🇸",
        "Profile": "https://gitlab.com/dennis",
        "Role": "Frontend Engineering Manager, Manage",
        "Last chat": "2022-06-15",
        "Count": "☕️☕️☕️☕️☕️"
    },
    {
        "Person": "Dimitrie Hoekstra 🇳🇱",
        "Profile": "https://gitlab.com/dimitrieh",
        "Role": "Product Designer Release::Progressive Delivery",
        "Last chat": "2020-10-22",
        "Count": "☕️"
    },
    {
        "Person": "Dirk de Vos 🇦🇺",
        "Profile": "https://gitlab.com/dadevos",
        "Role": "Director Channels & Alliances APAC at GitLab",
        "Last chat": "2021-12-16",
        "Count": "🍩"
    },
    {
        "Person": "Dmitry Gruzd 🇷🇺",
        "Profile": "https://gitlab.com/dgruzd",
        "Role": "Senior Backend Engineer, Global Search",
        "Last chat": "2020-09-01",
        "Count": "🍩"
    },
    {
        "Person": "Donald Cook 🇺🇸",
        "Profile": "https://gitlab.com/donaldcook",
        "Role": "Frontend Engineering Manager, Plan",
        "Last chat": "2020-09-10",
        "Count": "☕️"
    },
    {
        "Person": "Dorcas Aruwajoye 🇺🇸",
        "Profile": "https://gitlab.com/Daruwajoye",
        "Role": "Financial Planning and Analysis Manager",
        "Last chat": "2021-08-05",
        "Count": "🍩"
    },
    {
        "Person": "Elisabeth Posch 🇩🇪",
        "Profile": "https://gitlab.com/elisabethposch",
        "Role": "Account Executive",
        "Last chat": "2021-08-26",
        "Count": "🍩"
    },
    {
        "Person": "Emily Ring 🇺🇸",
        "Profile": "https://gitlab.com/emilyring",
        "Role": "Backend Engineer, Configure",
        "Last chat": "2020-07-21",
        "Count": "🍩"
    },
    {
        "Person": "Emily Bauman 🇨🇦",
        "Profile": "https://gitlab.com/emilybauman",
        "Role": "Product Designer, Growth",
        "Last chat": "2021-05-11",
        "Count": "☕️☕️"
    },
    {
        "Person": "Emily Murphy",
        "Profile": "https://gitlab.com/emilymurphy",
        "Role": "Manager, Sales Operations",
        "Last chat": "2021-05-12",
        "Count": "🍩"
    },
    {
        "Person": "Emily Sybrant 🇺🇸",
        "Profile": "https://gitlab.com/esybrant",
        "Role": "Product Designer, Fulfillment",
        "Last chat": "2021-08-18",
        "Count": "☕️"
    },
    {
        "Person": "Evan Read 🇦🇺",
        "Profile": "https://gitlab.com/eread",
        "Role": "Senior Technical Writer",
        "Last chat": "2021-09-09",
        "Count": "☕️"
    },
    {
        "Person": "Gabe Weaver 🇺🇸",
        "Profile": "https://gitlab.com/gweaver",
        "Role": "Senior Product Manager",
        "Last chat": "2021-02-08",
        "Count": "☕️"
    },
    {
        "Person": "George Tsiolis 🇬🇷",
        "Profile": "https://gitlab.com/gtsiolis",
        "Role": "Core team, Product Design at Gitpod",
        "Last chat": "2021-12-08",
        "Count": "☕️"
    },
    {
        "Person": "Gerardo Lopez-Fernandez 🇪🇸",
        "Profile": "https://gitlab.com/glopezfernandez",
        "Role": "Engineering Fellow, Infrastructure",
        "Last chat": "2020-08-06",
        "Count": "☕️"
    },
    {
        "Person": "Gerard Hickey 🇺🇸",
        "Profile": "https://gitlab.com/ghickey",
        "Role": "Senior Distribution Engineer",
        "Last chat": "2020-12-11",
        "Count": "🍩"
    },
    {
        "Person": "Gine Doyle 🇺🇸",
        "Profile": "https://gitlab.com/gdoyle",
        "Role": "Product Designer, Verify: Runner & Testing",
        "Last chat": "2021-07-21",
        "Count": "☕️"
    },
    {
        "Person": "Godwill N'Dulor 🇬🇧",
        "Profile": "https://gitlab.com/gndulor",
        "Role": "Strategic Account Leader",
        "Last chat": "2021-08-03",
        "Count": "🍩"
    },
    {
        "Person": "Hannes Moser 🇦🇹",
        "Profile": "https://gitlab.com/lamportsapprentice",
        "Role": "Staff Engineer, Incubation Engineering",
        "Last chat": "2022-04-26",
        "Count": "☕️"
    },
    {
        "Person": "Heather Simpson 🇺🇸",
        "Profile": "https://gitlab.com/heather",
        "Role": "Senior External Communications Analyst, Security",
        "Last chat": "2021-02-04",
        "Count": "🍩"
    },
    {
        "Person": "Hendriks Lopes 🇺🇸",
        "Profile": "https://gitlab.com/hendylopes",
        "Role": "Strategic Account leader - Boston",
        "Last chat": "2022-07-05",
        "Count": "🍩"
    },
    {
        "Person": "Holly Reynolds 🍑",
        "Profile": "https://gitlab.com/hollyreynolds",
        "Role": "Senior Product Designer, Plan",
        "Last chat": "2021-04-12",
        "Count": "☕️☕️"
    },
    {
        "Person": "Huzaifa Iftikhar 🇮🇳",
        "Profile": "https://gitlab.com/huzaifaiftikhar1",
        "Role": "Backend Engineer, Manage:Compliance",
        "Last chat": "2021-10-06",
        "Count": "☕️"
    },
    {
        "Person": "Iain Camcho 🇳🇱",
        "Profile": "https://gitlab.com/icamacho",
        "Role": "Sr. Product Designer, Package",
        "Last chat": "2021-06-16",
        "Count": "☕️"
    },
    {
        "Person": "Igor Drozdov 🇧🇾",
        "Profile": "https://gitlab.com/igor.drozdov",
        "Role": "Senior Backend Engineer, Create:Source Code",
        "Last chat": "2021-02-25",
        "Count": "☕️"
    },
    {
        "Person": "Jaclyn Grant 🇺🇸",
        "Profile": "https://gitlab.com/jngrant",
        "Role": "Sr. Executive Business Administrator",
        "Last chat": "2020-06-24",
        "Count": "🍩"
    },
    {
        "Person": "Jacki Bauer 🇺🇸",
        "Profile": "https://gitlab.com/jackib",
        "Role": "UX Manager, Enablement & Growth",
        "Last chat": "2021-08-24",
        "Count": "🍩🍩"
    },
    {
        "Person": "James Ramsay 🇦🇺",
        "Profile": "https://gitlab.com/jramsay",
        "Role": "Group Manager, Product",
        "Last chat": "2021-01-14",
        "Count": "☕️"
    },
    {
        "Person": "Jeff Crow 🇺🇸",
        "Profile": "https://gitlab.com/jeffcrow",
        "Role": "Senior UX Researcher, Growth",
        "Last chat": "2020-09-30",
        "Count": "🍩"
    },
    {
        "Person": "Jeny Bae 🍑",
        "Profile": "https://gitlab.com/jenybae",
        "Role": "Associate Sales Operations Analyst at Gitlab",
        "Last chat": "2021-04-15",
        "Count": "🍩"
    },
    {
        "Person": "Jen-Shin Lin 🇹🇼",
        "Profile": "https://gitlab.com/godfat",
        "Role": "Senior Backend Engineer, Engineering Productivity",
        "Last chat": "2020-07-02",
        "Count": "☕️"
    },
    {
        "Person": "Jennifer Lesile 🇺🇸",
        "Profile": "https://gitlab.com/JMLeslie",
        "Role": "PR Manager - Content",
        "Last chat": "2020-07-13",
        "Count": "🍩"
    },
    {
        "Person": "Jeremy Elder 🇺🇸",
        "Profile": "https://gitlab.com/jeldergl",
        "Role": "Senior Product Designer, UX Foundations",
        "Last chat": "2021-02-15",
        "Count": "☕️"
    },
    {
        "Person": "Jeremy Watson 🇺🇸",
        "Profile": "https://gitlab.com/jeremy",
        "Role": "Group Product Manager",
        "Last chat": "2020-08-24",
        "Count": "☕️"
    },
    {
        "Person": "Jessica Reeder 🇺🇸",
        "Profile": "https://gitlab.com/jessicareeder",
        "Role": "All-Remote Integrated Campaign Manager",
        "Last chat": "2020-08-27",
        "Count": "🍩"
    },
    {
        "Person": "Joe Drumtra 🇺🇸",
        "Profile": "https://gitlab.com/jdrumtra",
        "Role": "Strategic Account Leader",
        "Last chat": "2020-10-28",
        "Count": "🍩"
    },
    {
        "Person": "Jordi Mon Companys 🇬🇧",
        "Profile": "https://gitlab.com/mordo",
        "Role": "Senior Product Marketing Manager , Dev",
        "Last chat": "2020-07-01",
        "Count": "☕️"
    },
    {
        "Person": "Justin Boyson 🇺🇸",
        "Profile": "https://gitlab.com/jboyson",
        "Role": "Senior Frontend Engineer, Create:Source Code",
        "Last chat": "2020-10-01",
        "Count": "🍩"
    },
    {
        "Person": "Justin Mandell 🇺🇸",
        "Profile": "https://gitlab.com/jmandell",
        "Role": "Product Design Manager, Configure, Monitor, Secure & Defend",
        "Last chat": "2022-01-19",
        "Count": "🍩🍩🍩"
    },
    {
        "Person": "Katie Ramos 🇺🇸",
        "Profile": "https://gitlab.com/katieramos",
        "Role": "Sales Development Representative - Enterprise (East)",
        "Last chat": "2021-01-04",
        "Count": "☕️"
    },
    {
        "Person": "Katie Rogel 🌴",
        "Profile": "https://gitlab.com/krogel",
        "Role": "Field Marketing Coordinator",
        "Last chat": "2020-08-31",
        "Count": "☕️"
    },
    {
        "Person": "Katie Macoy 🇳🇿",
        "Profile": "https://gitlab.com/katiemacoy",
        "Role": "Product Designer,",
        "Last chat": "2022-01-04",
        "Count": "🍩🍩"
    },
    {
        "Person": "Kevin Comoli 🇳🇱",
        "Profile": "https://gitlab.com/kcomoli",
        "Role": "Product Designer, Growth:Conversion",
        "Last chat": "20201-11-15",
        "Count": "☕️🍩"
    },
    {
        "Person": "Lena Horal-Koretska 🇺🇦",
        "Profile": "https://gitlab.com/ohoral",
        "Role": "Senior Frontend Engineer, Ecosystem:Foundations",
        "Last chat": "2022-01-10",
        "Count": "☕️"
    },
    {
        "Person": "Libor Vanc 🇺🇸",
        "Profile": "https://gitlab.com/lvanc",
        "Role": "Senior Product Designer, Ecosystem",
        "Last chat": "2020-07-01",
        "Count": "☕️"
    },
    {
        "Person": "Lorie Whitaker 🇺🇸",
        "Profile": "https://gitlab.com/loriewhitaker",
        "Role": "Senior UX Researcher, Release and Verify & Enablement",
        "Last chat": "2020-08-20",
        "Count": "☕️"
    },
    {
        "Person": "Lukas Eipert 🇩🇪",
        "Profile": "https://gitlab.com/leipert",
        "Role": "Engineering Manager, Ecosystem:Foundations",
        "Last chat": "2022-01-27",
        "Count": "☕️"
    },
    {
        "Person": "Łukasz Korbasiewicz 🇨🇿",
        "Profile": "https://gitlab.com/lkorbasiewicz",
        "Role": "Support Engineer",
        "Last chat": "2021-03-23",
        "Count": "🍩"
    },
    {
        "Person": "Lyle Kozloff 🇨🇦",
        "Profile": "https://gitlab.com/lyle",
        "Role": "Senior Support Engineering Manager",
        "Last chat": "2020-10-06",
        "Count": "☕️"
    },
    {
        "Person": "Maggie Barnes 🇺🇸",
        "Profile": "https://gitlalb.com/m_barnes",
        "Role": "Sales Development Manager - Enterprise",
        "Last chat": "2020-08-11",
        "Count": "🍩"
    },
    {
        "Person": "Marcel van Remmerden 🇩🇪",
        "Profile": "https://gitlab.com/mvanremmerden",
        "Role": "Product Design Manager, Create",
        "Last chat": "2021-03-16",
        "Count": "☕️"
    },
    {
        "Person": "Marcin Sędłak-Jakubowski 🇵🇱",
        "Profile": "https://gitlab.com/msedlakjakubowski",
        "Role": "Technical Writer, Plan",
        "Last chat": "2021-10-25",
        "Count": "🍩🍩"
    },
    {
        "Person": "Maria Vrachni 🇬🇧",
        "Profile": "https://gitlab.com/mvrachni",
        "Role": "Senior Product Designer (Configure)",
        "Last chat": "2021-02-11",
        "Count": "☕️"
    },
    {
        "Person": "Mario de la Ossa 🇳🇮",
        "Profile": "https://gitlab.com/mdelaossa",
        "Role": "Backend Engineer, Plan:Project Management",
        "Last chat": "2020-07-29",
        "Count": "🍩"
    },
    {
        "Person": "Mark Florian 🇬🇧",
        "Profile": "https://gitlab.com/markrian",
        "Role": "Senior Frontend Engineer, Ecosystem:Foundations",
        "Last chat": "2022-01-18",
        "Count": "☕️"
    },
    {
        "Person": "Mark Wood 🇺🇸",
        "Profile": "https://gitlab.com/mjwood",
        "Role": "Senior Product Manager, Certify",
        "Last chat": "2020-09-09",
        "Count": "☕️"
    },
    {
        "Person": "Marta Kotek",
        "Profile": "https://gitlab.com/mkotek1",
        "Role": "Deal Desk Specialist",
        "Last chat": "2022-05-16",
        "Count": "🍩"
    },
    {
        "Person": "Mat Appelman 🇺🇸",
        "Profile": "https://gitlab.com/mappelman",
        "Role": "Principal Engineer, Monitor",
        "Last chat": "2022-01-04",
        "Count": "🍩"
    },
    {
        "Person": "Matej Latin 🇸🇮",
        "Profile": "https://gitlab.com/matejlatin",
        "Role": "Senior Product Designer, Growth",
        "Last chat": "2021-12-09",
        "Count": "☕️☕️"
    },
    {
        "Person": "Matt Gonzales 🇺🇸",
        "Profile": "https://gitlab.com/mattgonzales",
        "Role": "Senior Product Manager, Compliance",
        "Last chat": "2020-10-15",
        "Count": "☕️"
    },
    {
        "Person": "Max Woolf 🇬🇧",
        "Profile": "https://gitlab.com/mwoolf",
        "Role": "Senior Backend Engineer, Analyze:Product Analytics",
        "Last chat": "2023-01-23",
        "Count": "☕️☕️☕️☕️☕️☕️☕️☕️"
    },
    {
        "Person": "Melissa Ushakov 🇺🇸",
        "Profile": "https://gitlab.com/mushakov",
        "Role": "Group Manager, Product Management",
        "Last chat": "2022-01-12",
        "Count": "☕️"
    },
    {
        "Person": "Michael Fangman 🇺🇸",
        "Profile": "https://gitlab.com/mfangman",
        "Role": "Product Designer, Secure:Dynamic Analysis",
        "Last chat": "2021-04-20",
        "Count": "☕️"
    },
    {
        "Person": "Michael Le 🇦🇺",
        "Profile": "https://gitlab.com/mle",
        "Role": "Senior Product Designer - Static Site Editor",
        "Last chat": "2021-08-04",
        "Count": "🍩🍩"
    },
    {
        "Person": "Michael Preuss 🇨🇦",
        "Profile": "https://gitlab.com/mpreuss22",
        "Role": "Senior Manager, Digital Experience",
        "Last chat": "2020-07-20",
        "Count": "☕️"
    },
    {
        "Person": "Miguel Nunes 🇳🇱",
        "Profile": "https://gitlab.com/mnunes1",
        "Role": "Manager, Sales Development at Gitlab",
        "Last chat": "2021-10-13",
        "Count": "🍩"
    },
    {
        "Person": "Mike Greiling 🇺🇸",
        "Profile": "https://gitlab.com/mikegreiling",
        "Role": "Senior Frontend Engineer, Foundations",
        "Last chat": "2022-01-05",
        "Count": "☕️"
    },
    {
        "Person": "Mike Jang 🇺🇸",
        "Profile": "https://gitlab.com/mjang1",
        "Role": "Senior Technical Writer, Manage",
        "Last chat": "2021-04-20",
        "Count": "☕️☕️"
    },
    {
        "Person": "Monika Deshmukh 🇮🇳",
        "Profile": "https://gitlab.com/mdeshmukh1",
        "Role": "Associate Sales Operations Analyst at GitLab Inc",
        "Last chat": "2021-03-24",
        "Count": "🍩"
    },
    {
        "Person": "Nadia Udalova 🇳🇱",
        "Profile": "https://gitlab.com/nudalova",
        "Role": "Product Design Manager, CI/CD",
        "Last chat": "2020-09-04",
        "Count": "🍩"
    },
    {
        "Person": "Nate Spong 🇺🇸",
        "Profile": "https://gitlab.com/natespong",
        "Role": "Area Sales Manager - Federal Systems Integrators",
        "Last chat": "2020-06-23",
        "Count": "🍩"
    },
    {
        "Person": "Nick Brandt 🇺🇸",
        "Profile": "https://gitlab.com/nickbrandt",
        "Role": "Product Designer; Plan:Certify",
        "Last chat": "2023-01-18",
        "Count": "☕️☕️☕️☕️"
    },
    {
        "Person": "Nick Leonard",
        "Profile": "https://gitlab.com/nickleonard",
        "Role": "Senior Product Designer, Project Management",
        "Last chat": "2022-05-17",
        "Count": "☕️"
    },
    {
        "Person": "Nick Post 🇬🇧",
        "Profile": "https://gitlab.com/npost",
        "Role": "Senior Product Designer, Manage:Analytics",
        "Last chat": "2021-10-12",
        "Count": "☕️🍩🍩🍩☕️"
    },
    {
        "Person": "Nima Badiey 🇺🇸",
        "Profile": "https://gitlab.com/nbadiey",
        "Role": "VP of Alliances",
        "Last chat": "2021-07-15",
        "Count": "🍩"
    },
    {
        "Person": "Orit Golowinski 🇮🇱",
        "Profile": "https://gitlab.com/ogolowinski",
        "Role": "Group Manager, Product Management, Manage",
        "Last chat": "2021-05-19",
        "Count": "☕️"
    },
    {
        "Person": "Prashant Gupta 🇨🇦",
        "Profile": "https://gitlab.com/pgupta13",
        "Role": "Manager, Sales Commissions at GitLab",
        "Last chat": "2022-03-10",
        "Count": "🍩"
    },
    {
        "Person": "Patrick Deuley 🇺🇸",
        "Profile": "https://gitlab.com/deuley",
        "Role": "Senior Product Manager, Ecosystem",
        "Last chat": "2020-06-30",
        "Count": "☕️"
    },
    {
        "Person": "Patrick Steinhardt 🇩🇰",
        "Profile": "https://gitlab.com/pks-t",
        "Role": "Backend Engineer Gitaly",
        "Last chat": "2020-06-26",
        "Count": "☕️"
    },
    {
        "Person": "Pedro Moreira da Silva 🇵🇹",
        "Profile": "https://gitlab.com/pedroms",
        "Role": "Staff Product Designer",
        "Last chat": "2022-01-13",
        "Count": "🍩"
    },
    {
        "Person": "Peter Clitheroe 🇬🇧",
        "Profile": "https://gitlab.com/Clitheroe",
        "Role": "Area Sales Manager, SMB EMEA",
        "Last chat": "2022-04-05",
        "Count": "🍩"
    },
    {
        "Person": "Philip Joyce 🇮🇪",
        "Profile": "https://gitlab.com/philipjoyce",
        "Role": "Senior Product Designer, Fulfillment",
        "Last chat": "2022-06-27",
        "Count": "🍩"
    },
    {
        "Person": "Pranav Polisetty🇮🇳",
        "Profile": "https://gitlab.com/pranavpolisetty",
        "Role": "Senior SOX Compliance Analyst",
        "Last chat": "2021-05-19",
        "Count": "☕️"
    },
    {
        "Person": "Prathyusha Lakshmipuram 🇺🇸",
        "Profile": "https://gitlab.com/plakshmipuram",
        "Role": "Deal Desk Specialist",
        "Last chat": "2021-12-01",
        "Count": "🍩"
    },
    {
        "Person": "Rayana Verissimo 🇳🇱",
        "Profile": "https://gitlab.com/rayana",
        "Role": "Staff Product Designer, Verify",
        "Last chat": "20201-01-19",
        "Count": "☕️"
    },
    {
        "Person": "Rebecca Spainhower 🇺🇸",
        "Profile": "https://gitlab.com/rspainhower",
        "Role": "Support Engineering Manager, Americas",
        "Last chat": "2020-12-17",
        "Count": "☕️"
    },
    {
        "Person": "Rick Walker 🇺🇸",
        "Profile": "https://gitlab.com/rickwalker",
        "Role": "Strategic Account Leader West",
        "Last chat": "2021-03-08",
        "Count": "🍩"
    },
    {
        "Person": "Rob Hunt 🇬🇧",
        "Profile": "https://gitlab.com/rob.hunt",
        "Role": "Senior Frontend Engineer, Manage::Compliance",
        "Last chat": "2020-10-06",
        "Count": "☕️☕️"
    },
    {
        "Person": "Ron Chan 🇳🇿",
        "Profile": "https://gitlab.com/rchan-gitlab",
        "Role": "Senior Security Engineer, Application Security",
        "Last chat": "2020-07-29",
        "Count": "☕️"
    },
    {
        "Person": "Ross Hendrick 🇮🇪",
        "Profile": "https://gitlab.com/rhendrick",
        "Role": "People Operations Specialist",
        "Last chat": "2020-06-26",
        "Count": "☕️"
    },
    {
        "Person": "Rupert Douglas 🇬🇧",
        "Profile": "https://gitlab.com/rdouglas-gitlab",
        "Role": "Senior Technical Recruiter and intern UX Research Coordinator",
        "Last chat": "2020-07-17",
        "Count": "🍩"
    },
    {
        "Person": "Russell Dickenson 🇦🇺",
        "Profile": "https://gitlab.com/rdickenson",
        "Role": "Senior Technical Writer, Secure",
        "Last chat": "2022-01-19",
        "Count": "☕️"
    },
    {
        "Person": "Sam Beckham 🇬🇧",
        "Profile": "https://gitlab.com/samdbeckham",
        "Role": "Frontend Engineering Manager",
        "Last chat": "2023-01-31",
        "Count": "☕️"
    },
    {
        "Person": "Sam Kerr 🇺🇸",
        "Profile": "https://gitlab.com/stkerr",
        "Role": "Principal Product Manager",
        "Last chat": "2022-05-10",
        "Count": "☕️☕️"
    },
    {
        "Person": "Saumya Upadhyaya 🇮🇳",
        "Profile": "https://gitlab.com/supadhyaya",
        "Role": "Sr Product Marketing Manager",
        "Last chat": "2020-07-23",
        "Count": "☕️"
    },
    {
        "Person": "Sean Arnold 🇳🇿",
        "Profile": "https://gitlab.com/seanarnold",
        "Role": "Backend Engineer - Monitor",
        "Last chat": "2020-12-15",
        "Count": "🍩"
    },
    {
        "Person": "Sean John Hoyle 🇺🇸",
        "Profile": "https://gitlab.com/shoyle",
        "Role": "Technical Account Manager",
        "Last chat": "2021-12-08",
        "Count": "🍩☕️☕️"
    },
    {
        "Person": "Serena Fang 🇺🇸",
        "Profile": "https://gitlab.com/serenafang",
        "Role": "Backend Engineer - Manage:Access",
        "Last chat": "2021-05-27",
        "Count": "🍩"
    },
    {
        "Person": "Sid Sijbrandij 🇺🇸",
        "Profile": "https://gitlab.com/sytses",
        "Role": "CEO",
        "Last chat": "2020-10-14",
        "Count": "☕️"
    },
    {
        "Person": "Stefanie Inthachack",
        "Profile": "https://gitlab.com/sinthachack",
        "Role": "Mid-Market Account executive",
        "Last chat": "2022-04-20",
        "Count": "🍩"
    },
    {
        "Person": "Stephanie Binjour 🇺🇸",
        "Profile": "https://gitlab.com/sbinjour",
        "Role": "Legal Intern at Gitlab",
        "Last chat": "2021-09-27",
        "Count": "🍩"
    },
    {
        "Person": "Steve Abrams 🇺🇸",
        "Profile": "https://gitlab.com/sabrams",
        "Role": "Backend Engineer, Package",
        "Last chat": "2021-01-06",
        "Count": "🍩"
    },
    {
        "Person": "Sunjung Park 🇩🇪",
        "Profile": "https://gitlab.com/sunjungp",
        "Role": "Product Designer, Enablement",
        "Last chat": "2021-07-08",
        "Count": "🍩🍩"
    },
    {
        "Person": "Susan Tacker 🇺🇸",
        "Profile": "https://gitlab.com/susantacker",
        "Role": "Senior Manager Technical Writing",
        "Last chat": "2020-09-17",
        "Count": "🍩"
    },
    {
        "Person": "Suri Patel 🇺🇸",
        "Profile": "https://gitlab.com/suripatel",
        "Role": "Content Marketer",
        "Last chat": "2020-08-27",
        "Count": "☕️"
    },
    {
        "Person": "Tae Ho Hyun 🇰🇷",
        "Profile": "https://gitlab.com/taehohyun",
        "Role": "Strategic Account Leader",
        "Last chat": "2021-06-17",
        "Count": "🍩"
    },
    {
        "Person": "Tan Lee 🇦🇺",
        "Profile": "https://gitlab.com/tancnle",
        "Role": "Backend Engineer, Manage::Compliance",
        "Last chat": "2021-02-25",
        "Count": "☕️☕️"
    },
    {
        "Person": "Taurie Davis 🇺🇸",
        "Profile": "https://gitlab.com/tauriedavis",
        "Role": "Product Design Manager, Foundations",
        "Last chat": "2022-01-06",
        "Count": "☕️"
    },
    {
        "Person": "Terri Chu 🍑",
        "Profile": "https://gitlab.com/terrichu",
        "Role": "Senior Backend Engineer, Global Search Team",
        "Last chat": "20201-01-20",
        "Count": "🍩"
    },
    {
        "Person": "Thomas Hutterer",
        "Profile": "https://gitlab.com/thutterer",
        "Role": "Backend Engineer - Foundations",
        "Last chat": "2022-05-11",
        "Count": "☕️"
    },
    {
        "Person": "Tim Angelos",
        "Profile": "https://gitlab.com/tangelos",
        "Role": "DoD Area Sales Manager at GitLab Federal",
        "Last chat": "2021-10-25",
        "Count": "🍩"
    },
    {
        "Person": "Tim Noah 🇬🇧",
        "Profile": "https://gitlab.com/timnoah",
        "Role": "Senior Product Designer",
        "Last chat": "2021-07-08",
        "Count": "🍩"
    },
    {
        "Person": "Tim Poole 🇺🇸",
        "Profile": "https://gitlab.com/tpoole1",
        "Role": "Senior Analytics Engineer",
        "Last chat": "2021-11-22",
        "Count": "🍩"
    },
    {
        "Person": "Valerie Karnes 🇺🇸",
        "Profile": "https://gitlab.com/vkarnes",
        "Role": "Director of Product Design",
        "Last chat": "2022-01-18",
        "Count": "🍩"
    },
    {
        "Person": "Veethika Mishra 🇮🇳",
        "Profile": "https://gitlab.com/v_mishra",
        "Role": "Senior Product Designer, Verify:Continuous Integration",
        "Last chat": "2021-04-01",
        "Count": "🍩"
    },
    {
        "Person": "Yolanda Feldstein 🇺🇸",
        "Profile": "https://gitlab.com/yofeldstein",
        "Role": "Technical Account Manager (Customer Success)",
        "Last chat": "2020-08-04",
        "Count": "☕️"
    },
    {
        "Person": "Anastasiia Bakunovets 🇺🇦",
        "Profile": "https://gitlab.com/bakunovets",
        "Role": "Sales Development Representative",
        "Last chat": "2023-01-19",
        "Count": "🍩"

    },
    {

        "Person": "Julia Miocene 🇳🇱",
        "Profile": "https://gitlab.com/jmiocene",
        "Role": "Product Designer, Create:Editor",
        "Last chat": "2023-01-24",
        "Count": "🍩"
    }
    ],
    "caption" : "Let's have a ☕️ chat!"
}
```
