## WEEK 11 (2022), MILESTONE 14.9 → 14.10

### ⭐️ **Goal: Dig into backlog for Navigation**

> 7.1 hr in meetings (avg: 4.8 hr)

**Monday**
- [x] Meet with onboarding buddy
- [x] Respond to settings questions
- [x] Pajamas merge request maintainer reviews
- [x] Documentation merge request reviews
- [x] Finish [The Mom Test](https://www.amazon.com/Mom-Test-customers-business-everyone-ebook/dp/B01H4G2J1U)
- [x] Open a merge request on Font Color Standards
- [x] Quarterly earnings call
- [x] Document what GitLab Navigation does well according to NNG
- [x] Sandbox for navigation ideas

**Tuesday**
- [x] Mention responses
- [x] Merge request review for `gitlab-ui`
- [x] Read SUS verbatims on Navigation
- [x] Prep for 1:1 with Taurie
- [x] Identify 3 navigation opportunities
- [x] Prep for 1:1 with Christen
- [x] Reviewer question on colors
- [x] Mention responses
- [x] Brainstorming navigation ideas
- [x] MacOS update

**Wednesday**

- [x] Prep for UX Showcase
- [x] Sync meetings x6
- [x] Record video for using blame and share
- [x] Create merge request for empty tooltips
- [x] Feedback on User Research proposal

**Thursday**

- [x] Blank tooltip investigation
- [x] At least 5 To-do's
- [x] Sync meetings x2
- [x] Find several navgiation issues to move into the validation track

**Friday**
- [x] Try to resolve rspec testing (failed)
- [x] Look into alternative components for modals
- [x] Trainee maintainer responses
- [x] Close out lingering to-do's

---

### ♻️ Thoughts & Feelings

`Inbox 0` feels really challenging right now, but saying **no** is even harder to do.

### 🗄 Backlog

- [ ] Finish Shopify stuff
  - [ ] Get help with rspec testing
- [ ] Settings needs some backlog love
- [ ] Alerts improvements
- [ ] Unboxing the Advanced Section epic



