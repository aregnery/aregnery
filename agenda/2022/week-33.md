## WEEK 33 (2022), MILESTONE 15.3 - 15.4

### ⭐️ Goal: Get comfortable with being able to drop work if necessary

**Monday**

- [x] Record UX review of Deployments
- [x] Open issue to discuss changes to user management
- [x] Review direction merge request
- [x] Try the patch from Mark
- [x] ~~Look into concept testing data if available~~

**Tuesday**

- [x] 19 mentions ☠️
- [x] Review breakpoints on the environments change
- [x] Review menu menu proposal
- [x] UX weekly
- [x] Health webinar
- [x] More conversation updates
- [x] Watch nav findings preview

**Wednesday**

- [x] Word smith release post
- [x] Populate agendas
- [x] 4 sync meetings
- [x] Start working on navigation refinement
- [x] Reply to Christie's comment
- [x] Find Unfiltered video on the GlSearchFilter
- [x] Look into Observability suggestion

**Thursday**

- [x] Work on a few left sidebar ideas
- [x] Dig a little more into Observability
- [x] Review Anne's synthesis
- [x] Coffee chat
- [x] Sync with Nick

**Friday**

- [x] Work through project landing of nav concept
- [x] Schedule time with Nick on Monday
- [x] Watch and read about sophiscated simplicity
- [x] Help Veethika
- [x] Observaiblity investigation
- [x] Schedule round table with Anne, Taurie, and Nick
- [x] Try forking GitLab and deleting it on my personal project
