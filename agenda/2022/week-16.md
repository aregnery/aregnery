## WEEK 16 (2022), MILESTONE 15.0

> 7.5 hr in meetings (avg: 5.5 hr)

### ⭐️ Goal: Adjust research plan for card sorting accordingly

**Monday**
- [x] Move 14.10 merge requests into 15.0
- [x] Quartz Survey
- [x] Submit expense report
- [x] Sync meeting with Blair
- [x] Watch kickoff for %15.0
- [x] Add 14.10 merge requests to planning issue
- [x] Responses for todos
- [x] Review merge requests

**Tuesday**
- [x] Knock out easy todos
- [x] Populate agendas
- [x] Attempt another review for a MR
- [x] 14.10 retrospectives
- [x] Take notes for user research
- [x] Sync on card sorting
- [x] Look into 404 haml button

**Wednesday**
- [x] Build a personal issue for 15.0
- [x] Coffee chat
- [x] Sync validation track refinement
- [x] Monthly sync with Christen
- [x] Card sort outline refiement

**Thursday - Half day 🛫**
- [x] Small merge request review
- [x] Finish card sort outline
- [x] Pajamas matainer review
- [x] Sync with Christen
- [x] Slippers Design System check in
- [x] Finish some todos


**Friday - Vacation 🌴**


#### ♻️ Thoughts & Feelings

All the Shopify things merged this week! I've got a lot on my plate already. It's going to be a busy milestone.

#### 🗄 Backlog

- [ ] Update 15.0 milestone planning
- [ ] Quick action menus issue
- [ ] Contribute dropdown options ideas
- [ ] Drafts feature proposal
- [ ] Incorporate quick actions text feedback
- [ ] Pick 3 items for seeking community contributions
- [ ] Settings needs some backlog love
- [ ] Check in on alerts improvements
- [ ] Refine Unboxing the Advanced Section epic
