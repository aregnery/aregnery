## WEEK 12 (2022), MILESTONE 14.10

### ⭐️ Goal: Start grooming the validation track

> 5 hr in meetings (avg: 5 hr)

**Monday**
- [x] Meeting prep
- [x] Work on nav switcher ideas
- [x] Break down areas of the top nav menu
- [x] 5 sync meetings

**Tuesday**
- [x] Maintainer trainee retro
- [x] Graduation hat merge request
- [x] Alternate navigation ideas
- [x] Keep digging for gold in navigation issues

**Wednesday**
- [x] Look for some more lingering navigation ideas
- [x] Create merge request for milestone unboxing
- [x] Compliance design feedback

**Thursday**
- [x] Categorize project nav items
- [x] Research proposal feedback
- [x] Menu merge request review

**Friday**
- [x] Write down three smaller goals for navigation
- [x] Maintainer trainee work
- [x] Look into other product navigation structures

### ♻️ Thoughts & Feelings

Jumping into designing ideas for navigation is super hard. It can feel impossible to untangle what's been done already.

### 🗄 Backlog

- [ ] Pick 3 items for seeking community contributions
- [ ] Finish Shopify stuff
- [ ] Settings needs some backlog love
- [ ] Check in on alerts improvements
- [ ] Refine Unboxing the Advanced Section epic
