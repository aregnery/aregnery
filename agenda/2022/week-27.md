## WEEK 27 (2022), MILESTONE 15.2

### ⭐️ Goal: 4 shallow prototypes for pitching

**Monday**

- [x] 🇺🇸 US Holiday

**Tuesday**

- [x] Finish up toggle interaction from last week
- [x] Merge request reviews
- [x] Respond to @ mentions
- [x] Coffee chat
- [x] Fill out UX Research requests
- [x] Show how customization might happen
- [x] Start updating design 2

**Wednesday**

- [x] 5 sync meetings
- [x] Refine design 2 some more
- [x] Update research plan with thoughts


**Thursday**

- [x] UX review
- [x] Schedule a navigation proposal update for next Tueeday
- [x] Coffee chat ☕️
- [x] Watch [workspace videos](https://gitlab.com/gitlab-org/gitlab/-/issues/366202)
- [x] Update the project and homepage view of Sid's ideas
- [x] Work a bit on my 5th nav concept

**Friday**

- [x] Create agenda and update issue
- [x] Write the pitches
