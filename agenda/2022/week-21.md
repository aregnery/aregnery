## WEEK 21 (2022), MILESTONE 15.1

### ⭐️ Goal: Get 🍔 menu ideas into testing

**Monday**

- [x] Catchup on outstanding to-dos
- [x] UX review
- [x] Ideas for UX Showcase on the 25th
- [x] Sync on maintainer training
- [x] Post nav update

**Tuesday**

- [x] UX reviews
- [x] Respond to feedback on hmaburger menu
- [x] Incorporate ideas into global search and dropdown
- [x] Finish issue for UX Showcase
- [x] Populate agendas

**Wednesday**

- [x] More UX reviews
- [x] UX Showcase
- [x] 3 sync meetings
- [x] Refine designs further

**Thursday**

- [x] 2 sync meetings
- [x] Clean up to-do list
- [x] Work on dropdown prototype
- [x] Update vision prototype

**Friday**

- [x] 🌞 Family and Friends Day


#### ♻️ Thoughts & Feelings

I didn't quite get to setting up tests, but I feel good about what was accomplished.

#### 🗄 Backlog
- [ ] Pick 3 items for seeking community contributions
- [ ] Settings needs some backlog love
- [ ] Check in on alerts improvements
- [ ] Refine Unboxing the Advanced Section epic
