## WEEK 20 (2022), MILESTONE 15.0 → 15.1

### ⭐️ Goal: Post an update on Navigation progress

**Monday**

- [x] ☕️ chat x2
- [x] Open issue for feedback on Pajamas
- [x] Sync meetings x2
- [x] Incorporate sugesstion in merge request
- [x] Circle back to lingering 15.0 reviews
- [x] Pulse Survey
- [x] Security training
- [x] Help define plan for 15.1 issues
- [x] Take notes for UX research

**Tuesday**

- [x] ☕️ chat
- [x] Sync meeting x2
- [x] Watch key review again and look for feedback
- [x] Watch nav meeting feedback
- [x] Menu design 1
- [x] Menu design 2
- [x] Menu design 3
- [x] Menu design 4
- [x] Populate agendas for tomorrow

**Wednesday**

- [x] To-do replies
- [x] Sync meeting x4
- [x] ☕️ chat
- [x] Work on design feedback


**Thursday**

- [x] ☕️ chat x2
- [x] Sync with Nick Brandt
- [x] Tweak nav designs more
- [x] Merge request reviews x3
- [x] Knock out a few mentions


**Friday**

- [x] To-do list triage
- [x] UX reviews
- [x] More Figma work for 🍔 menu
- [x] Record navigation update


#### ♻️ Thoughts & Feelings

> I'm really tired, but feel like I accomplished a good bit. Need to be mindful of breaks.

#### 🗄 Backlog
- [ ] Ideas for UX Showcase on the 25th
- [ ] Pick 3 items for seeking community contributions
- [ ] Settings needs some backlog love
- [ ] Check in on alerts improvements
- [ ] Refine Unboxing the Advanced Section epic
