## WEEK 26 (2022), MILESTONE 15.2

### ⭐️ Goal: Establish themes for problems to navigation vision

**Monday**

- [x] Clean up prototype
- [x] 2 sync meetings
- [x] Read up on UX Roadmaps in the [handbook](https://about.gitlab.com/handbook/engineering/ux/product-design/ux-roadmaps/) and [NNG]](https://www.nngroup.com/articles/ux-roadmaps/)
- [x] 2 UX reviews
- [x] Rewatch Andy's UX Showcase
- [x] Navigation vision theme refinement

**Tuesday**

- [x] 4 UX reviews
- [x] Continue vision refinement into themes
- [x] 3 sync meetings

**Wednesday**

- [x] Triage to-do list
- [x] ☕️ Coffee chat
- [x] Synthesis for UX Research slide deck
- [x] Update Nick's design issue
- [x] Add tasks ideas for vision testing

**Thursday**

- [x] Work on exploration 3 prototype

**Friday**

- [x] 🌴 Vacation time

#### 🗄 Backlog

<details><summary>Ideas for another day</summary>

- [ ] Self reflect on personal development plan
- [ ] Pick 3 items for seeking community contributions
- [ ] Settings needs some backlog love
- [ ] Refine Unboxing the Advanced Section epic

</details>
