## WEEK 35 (2022), MILESTONE 15.4

### ⭐️ Goal: Have two prototype that _could_ be tested

**Monday**

- [x] 🌞 Family & Friends Day

**Tuesday**

- [x] Triage mentions
- [x] Fix Taurie's merge request
- [x] Review Observabiltiy again
- [x] Schedule coffee chat with Paul
- [x] Update coverage issue
- [x] Open merge requests for terminology consistency
- [x] Rotate personal access token
- [x] Merge requests for consistency
- [x] SUS feedback from FY23Q2

**Wednesday**

- [x] Respond to merge request mentions
- [x] Read SUS feedback again
- [x] Prep and attend quality meeting
- [x] Watch UX Showcases
- [x] Nav sync
- [x] Clean up prototype


**Thursday**

- [x] Add group / project landing pages
- [x] Prototype

**Friday**

- [x] Prototype
