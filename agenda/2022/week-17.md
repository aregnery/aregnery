## WEEK 17 (2022), MILESTONE 15.0

### ⭐️ Goal: Shape 15.0 work to highlight work

**Monday**
- [x] Catchup on 30+ todos
- [x] Email triage (Figma comments)
- [x] Update the shortcuts issue with a broader mockup
- [x] Link associated issues to shortcuts
- [x] Open merge request for box-shadow issue
- [x] Correspond with Taurie/Christen on Keynote Link
- [x] Contribute dropdown options ideas
- [x] Update 15.0 milestone planning

**Tuesday**
- [x] Coffee chat
- [x] Open issue for Figma update
- [x] Book order and expense report
- [x] Fix pipeline on MR
- [x] Finish sort order MR review
- [x] Try and fix pipeline again 😅
- [x] Nav revamp idea design


**Wednesday**
- [x] Listem item diff highlights
- [x] Coffee chat
- [x] Design 🍐 ideas for Annabel on threads
- [x] UX Showcase
- [x] Design 🍐 sync
- [x] Navigatin sync

**Thursday**
- [x] Heads down on Navigation

**Friday**
- [x] Heads down on Navigation
- [x] Knock down to-do's
- [x] Schedule meetings

#### ♻️ Thoughts & Feelings

Excited and nervous about Navigation. Trying to move quick, but be considerate.

#### 🗄 Backlog
- [ ] Drafts/discussion feature proposal
- [ ] Pick 3 items for seeking community contributions
- [ ] Settings needs some backlog love
- [ ] Check in on alerts improvements
- [ ] Refine Unboxing the Advanced Section epic
- [ ] Incorporate quick actions text feedback
- [ ] Next item on card sort issue
- [ ] Quick action menus issue
