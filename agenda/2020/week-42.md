### WEEK 42 (2020), MILESTONE 13.6 – W1
---
_I am going to be dogfood the iterations feature, so instead of documenting tasks that I want to accomplish outside of GitLab I am going to open an issue and assign it an iteration._

#### 🔁  Issues Tracker
These are tasks that take longer to-do and I try to plan ahead for, and I relate them to issues where most work is being done. I give each issue an assigned iteration (two-week block) and a due date within that. [Feedback issue](https://gitlab.com/gitlab-org/gitlab/-/issues/221284)
- [Iteration 10](https://gitlab.com/dashboard/issues?scope=all&utf8=%E2%9C%93&state=opened&assignee_username=aregnery&label_name[]=Iteration%3A%3A10) Oct 15 - 28
- [Iteration 11](https://gitlab.com/dashboard/issues?scope=all&utf8=%E2%9C%93&state=opened&assignee_username=aregnery&label_name[]=Iteration%3A%3A11) Oct 29 - Nov 11


<!-- 
My iteration template
```
/assign me
/label ~UX ~"Iteration::12" ~"group::compliance"
/due October 28
/iteration *iteration:"#12 (2020-11-12 - 2020-11-25)"
/weight 3
/award :bookmark:
``` 
-->








