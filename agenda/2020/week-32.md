### WEEK 32 (2020), MILESTONE [13.3](https://gitlab.com/gitlab-org/gitlab/-/analytics/issues_analytics?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=group%3A%3Acompliance&milestone_title=13.3&assignee_username=aregnery) – W3
- [x] Read more about the [360 feedback process](https://about.gitlab.com/handbook/people-group/360-feedback/)
- [x] Review feedback for UX Showcase
- [x] ~~Clean up proposals for #227628 & #220326~~
  - On hold in favor of a pivot
- [x] Update description for [Audit Events (Segmented Control)](https://gitlab.com/gitlab-org/gitlab/-/issues/223260/)
- [x] Continue with `P1` issues for `Next Up`
  - [x] Mockup + 📺 – Implementation: Adding two-person approvals for sensitive changes
  - [x] Mockup + 📺 – Implementation: Create API-based approval rules for merge request compliance checks
  - [x] Mockup: Export audit events to CSV
  - [x] `Start` Define custom compliance project labels for use with compliance pipeline configurations
- [ ] Migrate another button component
  - [x] Create aliases for GDK
  - [x] `Start` [!38939](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/38939)
  - [x] `Start` [!38966](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/38966)
- [x] ~Stectch Onboarding Video 📺
- [ ] ~Stretch Read through [&1385](https://gitlab.com/groups/gitlab-org/-/epics/1385) to better understand GitLab's UX History

~"Unplanned Tasks"
- [x] Find support for segmented control in other audit log tools
- [x] Watch Katherine's videos
  - [x] [Left Side Navigation](https://www.youtube.com/watch?v=ZeCdGTe_hbU&list=PL05JrBw4t0KrPNM6WFlrsVM8tWwkYDyio&index=2&t=0s)
  - [x] [UI Polish](https://www.youtube.com/watch?v=yLaqC_hb9vs&list=PL05JrBw4t0KrPNM6WFlrsVM8tWwkYDyio&index=3&t=0s)
  - [x] [Settings](https://www.youtube.com/watch?v=QTp41fngK18&list=PL05JrBw4t0KrPNM6WFlrsVM8tWwkYDyio&index=4&t=0s)
- [x] Read about [Achieve SOX Compliance Anytime with Segregation of Duties (SoD) Analyzer](https://saviynt.com/sod-analyzer/)
- [x] Review [manage vision](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/58727)
- [x] Checkout [async design crit](https://gitlab.com/gitlab-org/gitlab-design/-/issues/1296)
- [x] Put standing desk together (but its damaged 😭)
- [x] Rebuild GDK using data to populate the database with Compliance Dashboard info
- [x] Test `MR` [Export Merge commits CSV - Frontend changes](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/38513)

~"To-Do::Next Week"

- [ ] Fill out [Mural board for career growth](https://app.mural.co/t/gitlab2474/m/gitlab2474/1594045008555/7fed9cd40e226dda501db2c433bf364821998325) - complete by the 17th

~Backlog

- [ ] Read [Market Guide for Compliance Automation Tools in DevOps](https://drive.google.com/file/d/1Gt-zKKZVdaE62Hu_35HKy9uLXE2QKhvn/view)
- [ ] Open an issue to propose a change to the date picker component
- [ ] Open an issue to update what information is displayed in the breadcrumb
- [ ] Identify 1st time experience experiments for [#1192](https://gitlab.com/gitlab-org/gitlab-design/-/issues/1192) | &3839 (Approval Page)
- [ ] What is a feature flag?
- [ ] Create ~"UX debt" issues based on this [comment](https://gitlab.com/gitlab-org/gitlab/-/issues/219567#note_390783966)
- [ ] [Customize my terminal experience](https://medium.com/@caulfieldOwen/youre-missing-out-on-a-better-mac-terminal-experience-d73647abf6d7)






