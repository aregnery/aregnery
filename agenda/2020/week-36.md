### WEEK 36 (2020), MILESTONE [13.4](https://gitlab.com/gitlab-org/gitlab/-/analytics/issues_analytics?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=group%3A%3Acompliance&assignee_username=aregnery) – W4

- [ ] Figure out the Merge Request approvals inheritance problem
- [x] JTBD internal interviews
- [x] User Journey for Jira <> Merge Requests
- [x] Get to know the Certify Team

<!-- ~"Unplanned Tasks" -->

- [ ] Read the [GitLab.com Security Health Assessment](https://drive.google.com/file/d/1_luZCFnHz9T6SbIJZFhHjvLe9vuIDtvl/view)

<!-- ~"To-Do::Next Week" -->

~Backlog

- [ ] 📺 `Create` – Using GitLab to manage Compliance Policies & Controls
- [ ] Bringing Certify & Compliance together







