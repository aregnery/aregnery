### WEEK 34 (2020), MILESTONE [13.4](https://gitlab.com/gitlab-org/gitlab/-/analytics/issues_analytics?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=group%3A%3Acompliance&milestone_title=13.4&assignee_username=aregnery) – W1
  - [ ] `Finish` [!38939](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/38939)
    - [ ] Incorporate tertiary buttons
  - [ ] `Resolve` [!38966](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/38966)
    - [x] Implement Taurie's feedback
    - [x] Failing pipelines 👎
    - [x] Fix GDK
    - [ ] Fix broken tests
-  [x] Learn more about compliance / GitLab
    - [x] Read [Market Guide for Compliance Automation Tools in DevOps](https://drive.google.com/file/d/1Gt-zKKZVdaE62Hu_35HKy9uLXE2QKhvn/view)
    - [x] Cherry pick through [&1385](https://gitlab.com/groups/gitlab-org/-/epics/1385) to better understand GitLab's UX History
    - [x] What is a [feature flag](https://docs.gitlab.com/runner/configuration/feature-flags.html#feature-flags)?
- [x] Cherry pick issues that could be expedited through Solution Validation
- [x] Settle on a idea for [Group-level merge request approval rules](https://gitlab.com/gitlab-org/gitlab/-/issues/1111)
- [x] Put a simple idea forward for [Group-lvel push rules](https://gitlab.com/gitlab-org/gitlab/-/issues/221261)

~"Unplanned Tasks"
- [x] Open an issue to update what information is displayed in the breadcrumb
- [x] Create issue based on this [comment](https://gitlab.com/gitlab-org/gitlab/-/issues/219567#note_390783966)
- [x] Watch Product Weekly Meeting from `2020-08-18`

~"To-Do::Next Week"
-  GitLab Commit

~Backlog

- [ ] Identify 1st time experience experiments for [#1192](https://gitlab.com/gitlab-org/gitlab-design/-/issues/1192) | &3839 (Approval Page)
- [ ] [Brainstorm visionary ideas for Compliance](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/59216#note_393578986)
- [ ] 📺 Using GitLab to manage Compliance Documentation
- [ ] `P1` issues in `workflow::design`
- [ ] Put experiments into motion for issues in `workflow::solution validation`
- [ ] List goals for all issues I am assigned to for `13.4` and mark a health status










