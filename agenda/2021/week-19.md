## WEEK 19 (2021), MILESTONE 14.0

**Note to self**

There are a handful of KR's that I need to be intentional about contributing to for Q2.
- [KR: Focus on negative SUS theme related to merge requests](https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/11204)
- [KR: Continue validating Jobs to be Done to better support Category Maturity](https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/11205)

Am I actively participating? Partially. I still need to make sure we are moving in the right direction for CMS research in Q3. [Audit Reports](https://about.gitlab.com/direction/manage/compliance/audit-reports/) is expected to move up in maturity. 

We've got some collaboration in flight with the Pipeline Authoring group, and I need to get that buttoned up for a MVC. Long term this might be an ongoing collaboration with them till it is more robust.

Compliance Frameworks should really live somewhere else. I should either refine a proposal, or suggest just moving it.

Audit Events continues to grow in scope. Sam opened up some useful issues to identify a solution to validate. How should I prioritize that effort?

Merge Request widgets are going to be a key focus, so how might we still contribute our solution without negatively impacting the KR.

I really need to finish testing the source control user testing

Pedro left some good feedback on the help icon MR. I need to digest it, revise the MR, and run some testing on it.

Scaling up compliance features to Groups is still a thorn in my side. How can I rally others around that cause better?
