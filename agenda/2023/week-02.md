## WEEK 02 (2023), MILESTONE 15.8

Trying out a new prioritization matrix for each day
- 3 hours on a bigger task or goal
- 3 small tasks
- 3 maintenance chores

**Wednesday**

Big: transitioning back from parental leave
- [x] Catch up on Slack
- [x] Review FY23Q4 Product OKRs
- [x] Setup 1Password in Chrome

Small
- [x] Send thank you message
- [x] Order Yubikey
- [x] Schedule 3 coffee chats

Maintenance
- [x] Check HR systems for updates
- [x] Look for changes around the org


**Thursday**

Big: transitioning back from parental leave
- [x] Read What's New in GitLab
- [x] View Nick's Figma file
- [x] Review return issue
- [x] Read SUS verbatims
- [x] Watch UX Showcase on Navigation progress

Small
- [x] Listen to [Podcast episode on Figma](https://www.lennysnewsletter.com/p/an-inside-look-at-how-figma-builds?publication_id=10845&post_id=94678211#details)
- [x] Read docs page on GitLab Dedicated
- [x] Listen to GitLab Assembly
- [x] Schedule 3 more coffee chats

Maintenance / Chores
- [x] Get gdk running locally
- [x] Reachout to a CPA
- [x] Try to add SSH for verifying commits
- [x] Install GitLab CLI

**Backlog**

- [ ] `Chore` Convert coffee chats to JSON table
- [ ] `Big` Test the GlFilteredSearch component spike with feedback
- [ ] `Chore` Update my README
- [ ] `Maintenance` Reflect on what I want out of this year
- [ ] `Big` Explore GitLab Alpha features and look for changes
- [ ] `Big` Start triaging the To-Do List