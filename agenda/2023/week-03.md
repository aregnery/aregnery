## WEEK 03 (2023), MILESTONE 15.8

**Tuesday**

Big:
- [x] Look through font changes feedback issue
- [x] Sift through research testing results round 2
- [x] Look into Your Work sidebar feedback
- [x] Start triaging the To-Do List

Small:
- [x] Check tax on stock
- [x] Get Modern Health set up

Chore:
- [x] Coffee chat with Blair

**Wednesday**

Big:
- [x] Continue triaging my To-Do List
- [x] Sync meetings

Small:
- [x] Set up YubiKey
- [x] Update GDK

Chore:
- [x] Start converting coffee chats to JSON table
- [x] Coffee chats with Nick B. and Chris M.


**Thursday**

Big: 
- [x] Finish triaging my To-Do List
- [x] Inbox zero
- [x] Start documenting controversial or challenging proposals for navigation
- [x] Start digging through card sorting results

Small:
- [x] Finish converting coffee chats to JSON table
- [x] Security training

Chore:
- [x] Coffee chat



**Friday**
Big:
- [x] Look through research proposals
- [x] Start looking through epic roadmap for 15.9
- [x] Try to investigate the breadcrumb redirect for commits

Small:
- [x] Migrate TFA codes

Chore:
- [x] Reflect on what I want out of this year

---

**Backlog**

- [ ] `Chore` Update my README
